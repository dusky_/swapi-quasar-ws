export const MOCKED_PEOPLE = {
    'count': 2, 
    'next': null, 
    'previous': null, 
    'results': [
        {
            'name': 'Darth Vader', 
            'height': '202', 
            'mass': '136', 
            'hair_color': 'none', 
            'skin_color': 'white', 
            'eye_color': 'yellow', 
            'birth_year': '41.9BBY', 
            'gender': 'male', 
            'homeworld': 'https://swapi.py4e.com/api/planets/1/', 
            'films': [
                'https://swapi.py4e.com/api/films/1/', 
                'https://swapi.py4e.com/api/films/2/', 
                'https://swapi.py4e.com/api/films/3/', 
                'https://swapi.py4e.com/api/films/6/'
            ], 
            'species': [
                'https://swapi.py4e.com/api/species/1/'
            ], 
            'vehicles': [], 
            'starships': [
                'https://swapi.py4e.com/api/starships/13/'
            ], 
            'created': '2014-12-10T15:18:20.704000Z', 
            'edited': '2014-12-20T21:17:50.313000Z', 
            'url': 'https://swapi.py4e.com/api/people/4/'
        }, 
        {
            'name': 'Darth Maul', 
            'height': '175', 
            'mass': '80', 
            'hair_color': 'none', 
            'skin_color': 'red', 
            'eye_color': 'yellow', 
            'birth_year': '54BBY', 
            'gender': 'male', 
            'homeworld': 'https://swapi.py4e.com/api/planets/36/', 
            'films': [
                'https://swapi.py4e.com/api/films/4/'
            ], 
            'species': [
                'https://swapi.py4e.com/api/species/22/'
            ], 
            'vehicles': [
                'https://swapi.py4e.com/api/vehicles/42/'
            ], 
            'starships': [
                'https://swapi.py4e.com/api/starships/41/'
            ], 
            'created': '2014-12-19T18:00:41.929000Z', 
            'edited': '2014-12-20T21:17:50.403000Z', 
            'url': 'https://swapi.py4e.com/api/people/44/'
        }
    ]
}