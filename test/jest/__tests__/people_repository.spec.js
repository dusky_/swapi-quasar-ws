import axios from 'axios'
import { MOCKED_PEOPLE } from './mocks/mockedPeopleResponse';
import { PeopleRepository } from 'src/people/repository'

const PEOPLE_REPOSITORY = PeopleRepository(axios);

describe('People Repository', () => {
    describe('search', () => {
        it('return the response data', async () => {
            const DATA = await PEOPLE_REPOSITORY.search('Darth');
            expect(DATA).toStrictEqual(MOCKED_PEOPLE);
        });
    });

    describe('create, update, get, delete', () => { 
        it('should throw unimplemented exception', () => { 
            expect(() => PEOPLE_REPOSITORY.create()).toThrow('Method not implemented.');
            expect(() => PEOPLE_REPOSITORY.get()).toThrow('Method not implemented.');
            expect(() => PEOPLE_REPOSITORY.update()).toThrow('Method not implemented.');
            expect(() => PEOPLE_REPOSITORY.delete()).toThrow('Method not implemented.');
        })
    })
});
