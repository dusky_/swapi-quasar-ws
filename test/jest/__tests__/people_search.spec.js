/* eslint-disable @typescript-eslint/no-unsafe-call */
import { createLocalVue, mount } from '@vue/test-utils';
import PeopleSearch from 'src/people/components/people_search';
import * as storeModule from 'src/people/store/store';

describe('People search', () => {
    const localVue = createLocalVue();
    const wrapper = mount(PeopleSearch, { localVue });
    jest.useFakeTimers()

    describe('dInput', () => { 
        it('should dispatch debounced searchChanged method in store when value changes', () => { 
            const spy = jest.spyOn(storeModule, 'searchChanged');       
            wrapper.vm.$options.watch.dInput.call(wrapper.vm, 'G');
            wrapper.vm.$options.watch.dInput.call(wrapper.vm, 'GI');
            wrapper.vm.$options.watch.dInput.call(wrapper.vm, 'GIB');
            jest.advanceTimersByTime(150);
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith('GIB');
        });
    });
});

