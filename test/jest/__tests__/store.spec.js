import state, { exportForTest, searchChanged, getEnrichedNames } from 'src/people/store/store';
import { SERVICE_STATUS } from 'src/people/store/store_types';
import { MOCKED_PEOPLE } from './mocks/mockedPeopleResponse';

describe('Store', () => {
    beforeEach(() => { 
        state.people = [];
        state.searchQuery = '';
        state.status = SERVICE_STATUS.READY;
        jest.clearAllMocks();
    })

    describe('searchChanged', () => {
        it('should not call API and clear people in store when query is empty', async () => { 
            const spy = jest.spyOn(exportForTest.PEOPLE_REPOSITORY, 'search');
            state.people = ['Luke', 'Mocked Luke'];
            await searchChanged('');
            expect(spy).not.toBeCalled();
            expect(state.people).toStrictEqual([]);
        });

        it('should call serach API and set status to loading', () => { 
            const spy = jest.spyOn(exportForTest.PEOPLE_REPOSITORY, 'search');
            // eslint-disable-next-line @typescript-eslint/no-floating-promises
            searchChanged('Darth')
            expect(state.status).toEqual(SERVICE_STATUS.PENDING);
            expect(spy).toBeCalledWith('Darth');
        });

        it('should sets status to ready after it finished', async () => { 
            await searchChanged('Darth')
            expect(state.status).toEqual(SERVICE_STATUS.READY);
            expect(state.people).not.toStrictEqual([]);
        });

        it('should sets status to error if serach throws', async () => { 
            const spy = jest.spyOn(exportForTest.PEOPLE_REPOSITORY, 'search');
            spy.mockRejectedValue('Error');
            await searchChanged('Darth');
            expect(state.status).toEqual(SERVICE_STATUS.ERROR);
        });
    });

    describe('setServiceStatus', () => { 
        it('should sets store status from arguments', () => {
            expect(state.status).toEqual(SERVICE_STATUS.READY);            
            exportForTest.setServiceStatus(SERVICE_STATUS.ERROR);
            expect(state.status).toEqual(SERVICE_STATUS.ERROR);   
        });
    });
    
    describe('setPeople', () => { 
        it('should creates array of strings from response', () => { 
            expect(state.people).toStrictEqual([]);
            exportForTest.setPeople((MOCKED_PEOPLE));
            expect(state.people).toEqual(['Darth Vader', 'Darth Maul']);
        })
    }),

    describe('getEnrichedNames', () => { 
        it('should enrich people names by html', () => { 
            state.searchQuery = 'Darth';
            const EXPECTED_ENRICHED_NAMES = ['<b>Darth</b> Vader', '<b>Darth</b> Maul'];
            exportForTest.setPeople((MOCKED_PEOPLE));
            expect(state.people).toEqual(['Darth Vader', 'Darth Maul']);
            const ENRICHED_NAMES = getEnrichedNames();
            expect(ENRICHED_NAMES).toStrictEqual(EXPECTED_ENRICHED_NAMES);
        });
    })
});
