import { createLocalVue, mount } from '@vue/test-utils';
import PeopleList from 'src/people/components/people_list';
import state from 'src/people/store/store';
import { SERVICE_STATUS } from 'src/people/store/store_types';

describe('People list', () => {
    const localVue = createLocalVue();
    const wrapper = mount(PeopleList, { localVue });

    beforeEach(() => { 
        state.people = [];
        state.searchQuery = '';
        state.status = SERVICE_STATUS.READY;
        jest.clearAllMocks();
    })

    describe('isLoading', () => {
        it('returns true if state is loading', () => {
            state.status = SERVICE_STATUS.PENDING;
            expect(wrapper.vm.isLoading).toBe(true);
            state.status = SERVICE_STATUS.READY;
            expect(wrapper.vm.isLoading).toBe(false);
        });
    });

    describe('renderText', () => { 
        it('returns error text when status is error', () => { 
            state.status = SERVICE_STATUS.ERROR;
            expect(wrapper.vm.renderText).toEqual('There was an error with API. Please try again later.');
        });

        it('returns enter search phrase when search query is empty', () => { 
            state.searchQuery = '';
            state.status = SERVICE_STATUS.READY;
            expect(wrapper.vm.renderText).toEqual('Enter search phrase');
        });

        it('returns nothing found text if no people have been found', () => { 
            state.searchQuery = 'gibberish!';
            state.status = SERVICE_STATUS.READY;
            state.people = [];
            expect(wrapper.vm.renderText).toEqual('Nothing found');
        });

        it('returns nothing if there are people, API is not processing request and didn\'t fail with error', () => { 
            state.searchQuery = 'Darth';
            state.status = SERVICE_STATUS.READY;
            state.people = ['Darth Vader'];
            expect(wrapper.vm.renderText).toBe(null);
        });
    });
});

