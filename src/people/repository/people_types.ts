/* eslint-disable camelcase */
export interface StarWarsPersonInterface {
    name: string,
    birth_year: string,
    eye_color: string,
    gender: string,
    hair_color: string,
    height: string,
    mass: string,
    skin_color: string,
    homeworld: string,
    films: string[],
    species: string[],
    starships: string[],
    vehicles: string[],
    url: string,
    created: string,
    edited: string,
}

export interface StarWarsPeopleInterface {
    readonly count: string,
    readonly next: string | null,
    readonly previous: string | null,
    readonly results: StarWarsPersonInterface[]
}
