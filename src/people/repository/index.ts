import { StarWarsPersonInterface, StarWarsPeopleInterface } from './people_types'
import PeopleRepository from './people_repository'

export { StarWarsPersonInterface, StarWarsPeopleInterface, PeopleRepository }
