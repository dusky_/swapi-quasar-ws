import { AxiosInstance, AxiosResponse, AxiosError } from 'axios';
import { StarWarsPeopleInterface } from './people_types';

const BASE_URL = 'https://swapi.py4e.com/';
const API_ENDPOINT = `${BASE_URL}/api/people/`;

export default ($axios: AxiosInstance) => ({
  search(query: string): Promise<StarWarsPeopleInterface> {
    return $axios(`${API_ENDPOINT}/?search=${query}`)
      .then((response: AxiosResponse<StarWarsPeopleInterface>) => {
        const { data } = response;
        return data;
      })
      .catch((error: AxiosError) => {
        throw error;
      });
  },

  create() {
    throw new Error('Method not implemented.');
  },

  update() {
    throw new Error('Method not implemented.');
  },

  delete() {
    throw new Error('Method not implemented.');
  },

  get() {
    throw new Error('Method not implemented.');
  },
});
