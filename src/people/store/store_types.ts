export enum SERVICE_STATUS {
  'PENDING',
  'READY',
  'ERROR',
}

export interface StateInterface {
  people: string[];
  searchQuery: string;
  status: SERVICE_STATUS;
}
