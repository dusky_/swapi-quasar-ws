/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import Vue from 'vue';
import { PeopleRepository, StarWarsPeopleInterface } from '../repository';
import { StateInterface, SERVICE_STATUS } from '../store/store_types';
import axios from 'axios';

const PEOPLE_REPOSITORY = PeopleRepository(axios);

const state = Vue.observable<StateInterface>({ 
  people: [], 
  searchQuery: '', 
  status: SERVICE_STATUS.READY,
 }
);

export const searchChanged = async (query: string) => {
  state.searchQuery = query;
  if(query.length == 0) { 
    state.people = [];
    setServiceStatus(SERVICE_STATUS.READY);
    return;
  }

  setServiceStatus(SERVICE_STATUS.PENDING);
  await PEOPLE_REPOSITORY.search(query).then((people) => { 
      setPeople(people);
      setServiceStatus(SERVICE_STATUS.READY);
  }).catch(() => { 
      setServiceStatus(SERVICE_STATUS.ERROR);
  });
};

const setServiceStatus = (status: SERVICE_STATUS) => state.status = status;

const setPeople = (people: StarWarsPeopleInterface) => state.people = people.results.map((person) => person.name);

export const getEnrichedNames = () => state.people.map((person) => person.replace(new RegExp(state.searchQuery, 'gi'), (match) => `<b>${match}</b>`));

export default state;

export const exportForTest = {
  setServiceStatus,
  setPeople,
  PEOPLE_REPOSITORY,
}
